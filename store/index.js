import Vuex from "vuex";
import getTokenService from "~/plugins/getToken";

const createStore = () => {
  const tokenData = process.env.tokenData;
  return new Vuex.Store({
    state: {
      ambientePruebas: false,
      cargandocotizacion: false,
      msj: false,
      msjEje: false,
      sin_token: false,
      tiempo_minimo: 1.3,
      cotizacionesAmplia: [],
      cotizacionesLimitada: [],
      config: {
        time: 5000,
        reload: true,
        habilitarBtnEmision: false,
        habilitarBtnInteraccion: false,
        dataPeticionCotizacion: [],
        idProductoSolicitud: 0,
        idCotizacionNewCore: 0,
        tipoPaquete: 0,
        aseguradora: "HDI",
        cotizacion: false,
        emision: true,
        interaccion: true,
        descuento: 0,
        telefonoAS: "47495127",
        grupoCallback: "VN HDI",
        from: "DOMINIO-HDI",
        idPagina: 0,
        idMedioDifusion: 0,
        idMedioDifusionInteraccion: 0,
        idMedioDifusionEcommerce: 0,
        idAseguradora: 0,
        dominio: "seguroshdi.com",
        urlGracias: "",
        accessToken: "",
        cotizacionAli: "",
        desc: "",
        msi: "",
        promoLabel: "",
        promoImg: "",
        promoSpecial: false,
        extraMsg: "",
        logoAseguradora: "https://sfo3.digitaloceanspaces.com/prod-mx/prod_asmx/ahorraseguros.mx/wp-content/themes/nexoslab/assets/logotipos/hdi/logo.svg",
        agenteAutorizado: "https://sfo3.digitaloceanspaces.com/prod-mx/prod_asmx/ahorraseguros.mx/wp-content/themes/nexoslab/assets/logotipos/operado-por-ahorraguros.svg"
      },
      ejecutivo: {
        nombre: '',
        correo: '',
        id: 0
      },
      formData: {
        idHubspot:'',
        aseguradora: '',
        marca: '',
        modelo: '',
        descripcion: '',
        detalle: '',
        clave: '',
        cp: '',
        nombre: '',
        telefono: '',
        gclid_field: '',
        correo: '',
        edad: '',
        fechaNacimiento: '',
        genero: '',
        emailValid: '',
        telefonoValid: '',
        codigoPostalValid: '',
        urlOrigen: '',
        utmSource:'',
        utmMedium:'',
        utmCampaign:'',
        utmId:'',
        idLogData:'',
      },
      solicitud: {},
      cotizacion: {
        "cliente":{
           "tipoPersona":"",
           "nombre":"",
           "apellidoPat":"",
           "apellidoMat":"",
           "rfc":"",
           "fechaNacimiento":"",
           "ocupacion":"",
           "curp":"",
           "direccion":{
              "calle":"",
              "noExt":"",
              "noInt":"",
              "colonia":"",
              "codPostal":"",
              "poblacion":"",
              "ciudad":"",
              "pais":""
           },
           "edad":"28",
           "genero":"",
           "telefono":"",
           "email":""
        },
        "vehiculo":{
           "uso":"",
           "marca":"",
           "modelo":"",
           "noMotor":"",
           "noSerie":"",
           "noPlacas":"",
           "descripcion":"",
           "codMarca":"",
           "codDescripcion":"",
           "codUso":"",
           "clave":"2079479",
           "servicio":"",
           "subMarca":""
        },
        "coberturas":[
           
        ],
        "paquete":"AMPLIA",
        "descuento":"",
        "periodicidadDePago":"",
        "cotizacion":{
           "primaTotal":"",
           "primaNeta":"",
           "derechos":"",
           "impuesto":"",
           "recargos":"",
           "primerPago":"",
           "pagosSubsecuentes":"",
           "idCotizacion":"",
           "cotID":"",
           "verID":"",
           "cotIncID":"",
           "verIncID":"",
           "resultado":false
        },
        "emision":{
           "primaTotal":"",
           "primaNeta":"",
           "derechos":"",
           "impuesto":"",
           "recargos":"",
           "primerPago":"",
           "pagosSubsecuentes":"",
           "idCotizacion":0,
           "terminal":"",
           "documento":"",
           "poliza":0,
           "resultado":false
        },
        "pago":{
           "medioPago":"",
           "nombreTarjeta":"",
           "banco":"",
           "noTarjeta":"",
           "mesExp":"",
           "anioExp":"",
           "codigoSeguridad":"",
           "noClabe":"",
           "carrier":0,
           "msi":""
        },
        "codigoError":"",
        "urlRedireccion":"",
        "aseguradora":"HDI",
        "idCotizacion":""
      },
    },
    actions: {
      getToken() {
        return new Promise((resolve, reject) => {
          getTokenService.search(tokenData).then(
            (resp) => {
              if (typeof resp.data == "undefined") {
                this.state.config.accessToken = resp.accessToken;
              } else if (typeof resp.data.accessToken != "undefined") {
                this.state.config.accessToken = resp.data.accessToken;
              }
              localStorage.setItem("authToken", this.state.config.accessToken);
              resolve(resp);
            },
            (error) => {
              reject(error);
            }
          );
        });
      },
    },
    mutations: {
      validarTokenCore: function (state) {
        try {
          if (process.browser) {
            if (localStorage.getItem("authToken") === null) {
              state.sin_token = true;
              console.log("NO HAY TOKEN...");
            } else {
              console.log("VALIDANDO TOKEN...");
              state.config.accessToken = localStorage.getItem("authToken");
              var tokenSplit = state.config.accessToken.split(".");
              var decodeBytes = atob(tokenSplit[1]);
              var parsead = JSON.parse(decodeBytes);
              var fechaUnix = parsead.exp;
              /*
               * Fecha formateada de unix a fecha normal
               * */
              var expiracion = new Date(fechaUnix * 1000);
              var hoy = new Date(); //fecha actual
              /*
               * Se obtiene el tiempo transcurrido en milisegundos
               * */
              var tiempoTranscurrido = expiracion - hoy;
              /*
               * Se obtienen las horas de diferencia a partir de la conversión de los
               * milisegundos a horas.
               * */
              var horasDiferencia =
                Math.round(
                  (tiempoTranscurrido / 3600000 + Number.EPSILON) * 100
                ) / 100;

              if (hoy > expiracion || horasDiferencia < state.tiempo_minimo) {
                state.sin_token = "expirado";
              }
            }
          }
        } catch (error2) {
          console.log(error2);
        }
      },
    },
  });
};
export default createStore;
