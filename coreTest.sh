#! /bin/bash
grep -rl 'https://dev.' nuxt.config.js | xargs sed -i 's/https:\/\/dev./https:\/\//g'
grep -rl 'https://p.' nuxt.config.js | xargs sed -i 's/https:\/\/p./https:\/\//g'
grep -rl 'DEVELOP' nuxt.config.js | xargs sed -i 's/DEVELOP/PRODUCTION/g'
grep -rl '_blank")' components/utilities/btnsEcomerce.vue | xargs sed -i 's/_blank\")/_self\")/g'

# HUBSPOT
#grep -rl 'hubspot-dev' nuxt.config.js | xargs sed -i 's/hubspot-dev/hubspot-prod/g'