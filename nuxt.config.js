module.exports = {
  target: "static",
  /*
   ** Headers of the page
   */
  head: {
    title: "seguroshdi.com",
    htmlAttrs: { lang: "es-MX" },
    meta: [
      { charset: "utf-8" },
      { hid: "robots", name: "robots", content: "index, follow" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "ANA" },
    ],
    link: [{ rel: "icon", type: "image/png", href: "/faviconH.png" }],
  },
  /*
   ** Customize the progress bar color
   */
  loading: { color: "#3bd600" },
  /*
   ** Build configuration
   */
  build: {
    /*
     ** Run ESLint on save
     */
    extend(config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: "pre",
          test: /\.(js|vue)$/,
          exclude: /(node_modules)/,
        });
      }
    },
  },
  css: ["static/css/bootstrap.min.css", "static/css/styles.css"],
  plugins: [
    { src: "~/plugins/filters.js", ssr: false },
    { src: "~/plugins/apm-rum.js", ssr: false },
  ],
  modules: ["@nuxtjs/axios"],
  gtm: { id: "GTM-PFPTG4G" },
  env: {
    tokenData: "2/SfiAuJmICCW2WurfZsVbTvNsjLtrWdQ3gsHLqLSjE=", // TOKEN DATA
    catalogo: "https://dev.ws-hdiseguro.com", // CATALOGO
    coreBranding: "https://dev.core-brandingservice.com", // CORE
    //motorCobro: "https://p.seguroshdi.com/compra", // MOTOR COBRO
    motorCobro: "https://p.seguroshdi.com/compra/cliente", // MOTOR COBRO
    sitio: "https://p.seguroshdi.com", // SITIO
    urlValidaciones: "https://core-blacklist-service.com/rest", // VALIDACIONES
    urlMonitoreo: "https://core-monitoreo-service.com/v1", // MONITOREO
    promoCore: "https://dev.core-persistance-service.com",
    hubspot:"https://core-hubspot-dev.mark-43.net/deals/landing",    
    Environment: "DEVELOP",
  },
  render: {
    http2: { push: true },
    resourceHints: false,
    compressor: { threshold: 9 },
  },
};
